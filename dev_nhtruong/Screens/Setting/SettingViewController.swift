//
//  SettingViewController.swift
//  dev_nhtruong
//
//  Created by Huu Truong Nguyen on 2/7/22.
//

import UIKit

class SettingViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var speedLabel: UILabel!
    @IBOutlet weak var showSpeed: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var speedSlider: UISlider!
    @IBOutlet weak var sizeSlider: UISlider!
    @IBOutlet weak var shareView: UIView!
    @IBOutlet weak var shareButton: UIButton!

    @IBAction func changeValueSize(_ sender: Any) {
        titleLabel.font = .systemFont(ofSize: CGFloat(sizeSlider.value)*1.5*50)
        speedLabel.font = .systemFont(ofSize: CGFloat(sizeSlider.value)*50)
        sizeLabel.font = .systemFont(ofSize: CGFloat(sizeSlider.value)*50)
        showSpeed.font = .systemFont(ofSize: CGFloat(sizeSlider.value)*50)
    }
    
    @IBAction func changeValueSpeed(_ sender: Any) {
        setSpeedLabel()
    }
    
    @IBAction func share(_ sender: UIButton) {
        let text = [ "https://apps.apple.com/us/app/id" + appid ]
        let activityViewController = UIActivityViewController(activityItems: text , applicationActivities: nil)
        if isIPad {
            if activityViewController.responds(to: #selector(getter: UIViewController.popoverPresentationController)) {
                activityViewController.popoverPresentationController?.sourceView = sender
            }
        }
        activityViewController.hidesBottomBarWhenPushed = true
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sizeSlider.minimumValue = 0.2
        sizeSlider.maximumValue = 0.8
        speedSlider.minimumValue = 0.1
        speedSlider.maximumValue = 1.5
        shareView.layer.cornerRadius = 10
        shareView.shadowView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        titleLabel.font = .systemFont(ofSize: CGFloat(letterSize!)*1.5)
        speedLabel.font = .systemFont(ofSize: CGFloat(letterSize!))
        sizeLabel.font = .systemFont(ofSize: CGFloat(letterSize!))
        sizeSlider.value = Float(letterSize!/50.0)
        speedSlider.value = Float(readSpeed!)
        setSpeedLabel()
    }

    func setSpeedLabel() {
        let speed = Double(speedSlider.value).rounded(toPlaces: 2)
        showSpeed.text = String(speed) + "x"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        readSpeed = Double(speedSlider.value)
        letterSize = Double(sizeSlider.value*50)

        UserDefaults.standard.set(speedSlider.value, forKey: "SPEED")
        UserDefaults.standard.set(sizeSlider.value*50, forKey: "SIZE")
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
