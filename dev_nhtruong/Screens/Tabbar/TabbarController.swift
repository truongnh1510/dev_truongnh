//
//  TabbarController.swift
//  dev_nhtruong
//
//  Created by Huu Truong Nguyen on 2/7/22.
//

import UIKit

class TabbarController: UITabBarController, UINavigationControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        //interactivePopGestureRecognizer?.isEnabled = true
        viewControllers = [makeNavigationDictionary(),
                           makeNavigationFavorite(),
                           makeNavigationSetting()]
//        viewControllers = [DictionaryViewController(),
//                           SettingViewController(),
//                           FavoriteViewController()]
        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func makeNavigationDictionary() -> BaseNavigationController {
        let vc = DictionaryViewController()
        vc.tabBarItem = UITabBarItem(title: "Dictionary", image: #imageLiteral(resourceName: "languages"), selectedImage: #imageLiteral(resourceName: "languages"))
        let navi = BaseNavigationController(rootViewController: vc)
        navi.setNavigationBarHidden(true, animated: false)
        return navi
    }
    
    func makeNavigationSetting() -> BaseNavigationController {
        let vc = SettingViewController()
        vc.tabBarItem = UITabBarItem(title: "Setting", image: #imageLiteral(resourceName: "admin"), selectedImage: #imageLiteral(resourceName: "admin"))
        let navi = BaseNavigationController(rootViewController: vc)
        navi.setNavigationBarHidden(true, animated: false)
        return navi
    }
    
    func makeNavigationFavorite() -> BaseNavigationController {
        let vc = FavoriteViewController()
        vc.tabBarItem = UITabBarItem(title: "Favorite", image: #imageLiteral(resourceName: "lover"), selectedImage: #imageLiteral(resourceName: "lover"))
        let navi = BaseNavigationController(rootViewController: vc)
        navi.setNavigationBarHidden(true, animated: false)
        return navi
    }
}
