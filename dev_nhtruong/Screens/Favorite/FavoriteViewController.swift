//
//  FavoriteViewController.swift
//  dev_nhtruong
//
//  Created by Huu Truong Nguyen on 2/7/22.
//

import UIKit

class FavoriteViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var listData: [Word] = [Word]()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(LOAD_DATABASE_DONE), name: Notification.Name("LOAD_DATABASE_DONE"), object: nil)
        tableView.register(UINib(nibName: "WordTBVCell", bundle: nil), forCellReuseIdentifier: "WordTBVCell")
        // Do any additional setup after loading the view.
    }
    
    func getDataFavorite() -> [Word]{
        var data: [Word] = [Word]()
        for item in listData {
            if item.favorite == 1 {
                data.append(item)
            }
        }
        return data
    }
    
    @objc func LOAD_DATABASE_DONE(notification: Notification) {
        listData = SQLService.shared.listDataDetail
        DispatchQueue.main.async {
            self.titleLabel.font = .systemFont(ofSize: CGFloat(letterSize!)*1.5)
            self.tableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        titleLabel.font = .systemFont(ofSize: CGFloat(letterSize!)*1.5)
        listData = SQLService.shared.listDataDetail
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FavoriteViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.getDataFavorite().count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(letterSize!*6)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WordTBVCell", for: indexPath) as! WordTBVCell
        cell.word.text = getDataFavorite()[indexPath.row].word
        cell.mean.text = getDataFavorite()[indexPath.row].mean
        cell.mean.font = .systemFont(ofSize: CGFloat(letterSize!))
        cell.word.font = .systemFont(ofSize: CGFloat(letterSize!))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        tableView.deselectRow(at: indexPath, animated: true)
        //EnglishService.shared.updateRecent(id: listData[indexPath.row].id)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DetailVC") as! DetailVC
        //let vc = DetailVC()
        vc.wordText = getDataFavorite()[indexPath.row].word
        vc.meanText = getDataFavorite()[indexPath.row].mean
        vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        self.navigationController?.pushViewController(vc, animated: true)
        //self.present(vc, animated: true, completion: nil)
        }
        
    }
