//
//  WordTBVCell.swift
//  dev_nhtruong
//
//  Created by Huu Truong Nguyen on 2/7/22.
//

import UIKit

class WordTBVCell: UITableViewCell {
    @IBOutlet weak var word: UILabel!
    @IBOutlet weak var mean: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
