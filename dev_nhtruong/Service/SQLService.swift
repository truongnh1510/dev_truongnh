//
//  SQLService.swift
//  dev_nhtruong
//
//  Created by Huu Truong Nguyen on 2/7/22.
//

import Foundation
import SQLite

class SQLService: NSObject {
    static let shared: SQLService = SQLService()
    var DatabaseRoot: Connection?
    var listDataDetail: [Word] = [Word]()
    let Detail = Table("word_tbl")
    var word = Expression<String?>("word")
    let mean = Expression<String?>("mean")
    let favorite = Expression<Int>("favorite")

    func loadInit(linkPath: String){
        var dbPath : String = ""
        var dbResourcePath : String = ""
        
        let fileManager = FileManager.default
        do{
            dbPath = try fileManager
                .url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
                .appendingPathComponent("ev.db")
                .path
            if !fileManager.fileExists(atPath: dbPath) {
                dbResourcePath = Bundle.main.path(forResource: "ev", ofType: "db")!
                try fileManager.copyItem(atPath: dbResourcePath, toPath: dbPath)
            }
        }catch{
            print("An error has occured")
        }
        do {
            self.DatabaseRoot = try Connection (dbPath)
        } catch {
            print(error)
        }
    }

    func getDataDetail(closure: @escaping (_ response: [Word]?, _ error: Error?) -> Void) {
        
        listDataDetail.removeAll()
        if let DatabaseRoot = DatabaseRoot{
            do{
                for user in try DatabaseRoot.prepare(Detail) {
                    listDataDetail.append(Word(word: user[word] ?? "", mean: user[mean] ?? "", favorite: user[favorite]))
                }
            } catch  {
            }
        }
        closure(listDataDetail, nil)
        NotificationCenter.default.post(name: Notification.Name("LOAD_DATABASE_DONE"), object: nil)
    }
    
    func markedFavourite(word: String){
        for item in self.listDataDetail{
            if item.word == word {
                item.favorite = 1
            }
        }
        let filter = Detail.filter(self.word == word)
        do{
            let updateEnglish0 = filter.update(self.favorite <- 1)
            try DatabaseRoot!.run(updateEnglish0)
        } catch{
            print("Update failed: \(error)")
        }
    }
    
    func unmarkedFavourite(word: String){
        for item in self.listDataDetail{
            if item.word == word {
                item.favorite = 0
            }
        }
        let filter = Detail.filter(self.word == word)
        do{
            let updateEnglish0 = filter.update(self.favorite <- 0)
            try DatabaseRoot!.run(updateEnglish0)
        } catch{
            print("Update failed: \(error)")
        }
    }
    
    func didMarkedFavourite(word: String)->Bool{
        for item in self.listDataDetail{
            if item.word == word {
                if item.favorite == 0 {
                    return false
                }else{
                    return true
                }
            }
        }
        return false
    }
}

