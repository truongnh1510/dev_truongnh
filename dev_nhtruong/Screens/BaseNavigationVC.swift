//
//  BaseNavigationVC.swift
//  dev_nhtruong
//
//  Created by Huu Truong Nguyen on 2/7/22.
//

import UIKit

class BaseNavigationController: UINavigationController, UIGestureRecognizerDelegate {
    
    // MARK: - Override
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
    }
}

// MARK: - Private Functions
extension BaseNavigationController {
    
    private func setupNavigationBar() {
       // let attribute = [NSAttributedString.Key.foregroundColor: UIColor.black,
                         //NSAttributedString.Key.font: UIFont.avertaStdCY(weight: .semiBold, size: 18)]
//        navigationBar.do {
////            $0.barTintColor = UIColor.backgroundColor
//            $0.isTranslucent = true
//            $0.shadowImage = UIImage()
//            $0.titleTextAttributes = attribute
//            $0.backIndicatorImage = #imageLiteral(resourceName: "ic_back")
////            $0.tintColor = UIColor.backgroundColor
//            $0.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
//            self.navigationController?.view.backgroundColor = UIColor.clear
//        }
        delegate = self
        interactivePopGestureRecognizer?.delegate = self
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        super.pushViewController(viewController, animated: true)
        //viewController.removeBackButtonTitle()
    }
}

// MARK: - Navigation delegate
extension BaseNavigationController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController,
                              didShow viewController: UIViewController, animated: Bool) {
        if viewControllers.count > 1 {
            interactivePopGestureRecognizer?.isEnabled = true
        } else {
            interactivePopGestureRecognizer?.isEnabled = false
        }
    }
}
