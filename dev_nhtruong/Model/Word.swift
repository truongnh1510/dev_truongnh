//
//  Word.swift
//  dev_nhtruong
//
//  Created by Huu Truong Nguyen on 2/7/22.
//

import Foundation

class Word {
    var word: String = ""
    var mean: String = ""
    var favorite: Int = 0
    
    init(word: String, mean: String, favorite: Int){
        self.word = word
        self.mean = mean
        self.favorite = favorite
    }
}
