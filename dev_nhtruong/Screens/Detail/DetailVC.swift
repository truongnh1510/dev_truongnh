//
//  DetailVC.swift
//  dev_nhtruong
//
//  Created by Huu Truong Nguyen on 2/7/22.
//

import UIKit
import AVFoundation

class DetailVC: UIViewController {
    @IBOutlet weak var word: UILabel!
    @IBOutlet weak var mean: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var speakerButton: UIButton!

    var wordText = ""
    var meanText = ""
    
    @IBAction func back() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func favorite() {
        let isFavorite = SQLService.shared.didMarkedFavourite(word: wordText)
        if isFavorite {
            SQLService.shared.unmarkedFavourite(word: wordText)
            favoriteButton.setImage(#imageLiteral(resourceName: "love"), for: .normal)
        }
        else {
            SQLService.shared.markedFavourite(word: wordText)
            favoriteButton.setImage(#imageLiteral(resourceName: "lover"), for: .normal)
        }
    }
    
    @IBAction func speak() {
        let utterance = AVSpeechUtterance(string: wordText)

        // Configure the utterance.
        utterance.rate = Float(readSpeed!)
//        utterance.pitchMultiplier = 0.8
//        utterance.postUtteranceDelay = 0.2
        //utterance.volume = 0.8

        // Retrieve the British English voice.
        let voice = AVSpeechSynthesisVoice(language: "en-US")

        // Assign the voice to the utterance.
        utterance.voice = voice
        let synth = AVSpeechSynthesizer()
        synth.speak(utterance)
        setDisableButton()
    }

    func setDisableButton() {
        speakerButton.alpha = 0.5
        speakerButton.isUserInteractionEnabled = false
        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(setEnableButton), userInfo: nil, repeats: false)
    }
    
    @objc func setEnableButton() {
        speakerButton.alpha = 1
        speakerButton.isUserInteractionEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        word.font = .systemFont(ofSize: CGFloat(letterSize!)*1.5)
        mean.font = .systemFont(ofSize: CGFloat(letterSize!))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        word.text = wordText
        mean.text = meanText
        let isFavorite = SQLService.shared.didMarkedFavourite(word: wordText)
        if isFavorite {
            favoriteButton.setImage(#imageLiteral(resourceName: "lover"), for: .normal)
        }
        else {
            favoriteButton.setImage(#imageLiteral(resourceName: "love"), for: .normal)
        }
        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
