//
//  Extension.swift
//  dev_nhtruong
//
//  Created by Huu Truong Nguyen on 2/10/22.
//

import Foundation
import UIKit

extension UIView {
    func roundCorners(_ corners: UIRectCorner,
                      radius: CGFloat,
                      borderColor: UIColor = .clear,
                      borderWidth: CGFloat = 2,
                      shadowColor: UIColor = .black) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
        
        let borderLayer = CAShapeLayer()
        borderLayer.name = "corner"
        borderLayer.path = path.cgPath
        borderLayer.lineWidth = borderWidth
        borderLayer.strokeColor = borderColor.cgColor
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.frame = mask.bounds
        layer.addSublayer(borderLayer)
    }
    
    func shadowView(color: UIColor = .black,
                    alpha: Float = 0.25,
                    x: CGFloat = 0,
                    y: CGFloat = 3,
                    blur: CGFloat = 8) {
        layer.shadowColor = color.cgColor
        layer.shadowOffset = CGSize(width: x, height: y)
        layer.shadowOpacity = alpha
        layer.shadowRadius = blur
        layer.masksToBounds = false
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
