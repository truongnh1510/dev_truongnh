//
//  AppDelegate.swift
//  dev_nhtruong
//
//  Created by Huu Truong Nguyen on 2/7/22.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        if let pathForFile = Bundle.main.path(forResource: "ev", ofType: "db"){
            SQLService.shared.loadInit(linkPath: pathForFile)
            SQLService.shared.getDataDetail(){ respond, error in
            }
        }
        
        if readSpeed == nil {
            UserDefaults.standard.set(0.5, forKey: "SPEED")
        }
        if letterSize == nil {
            UserDefaults.standard.set(17, forKey: "SIZE")
        }
//        window = UIWindow.init(frame: UIScreen.main.bounds)
//        let tabBarVC = TabbarController()
//
//        var nav1 = UINavigationController()
//        nav1.viewControllers = [tabBarVC]
//
//        window?.rootViewController = nav1
//        window?.makeKeyAndVisible()
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

