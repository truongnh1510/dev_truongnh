//
//  DictionaryViewController.swift
//  dev_nhtruong
//
//  Created by Huu Truong Nguyen on 2/7/22.
//

import UIKit

class DictionaryViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var listData: [Word] = [Word]()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "WordTBVCell", bundle: nil), forCellReuseIdentifier: "WordTBVCell")
//        SQLService.shared.getDataDetail(){ respond, error in
//            if let respond = respond {
//                self.listData = respond
//                DispatchQueue.main.async {
//                    self.tableView.reloadData()
//                }
//            }
//        }
        NotificationCenter.default.addObserver(self, selector: #selector(LOAD_DATABASE_DONE), name: Notification.Name("LOAD_DATABASE_DONE"), object: nil)
        // Do any additional setup after loading the view.
    }

    @objc func LOAD_DATABASE_DONE(notification: Notification) {
        listData = SQLService.shared.listDataDetail
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        listData = SQLService.shared.listDataDetail
        DispatchQueue.main.async {
            self.titleLabel.font = .systemFont(ofSize: CGFloat(letterSize!)*1.5)
            self.tableView.reloadData()
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

extension DictionaryViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(letterSize!*6)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WordTBVCell", for: indexPath) as! WordTBVCell
        cell.mean.font = .systemFont(ofSize: CGFloat(letterSize!))
        cell.word.font = .systemFont(ofSize: CGFloat(letterSize!))
        cell.word.text = listData[indexPath.row].word
        cell.mean.text = listData[indexPath.row].mean
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        tableView.deselectRow(at: indexPath, animated: true)
        //EnglishService.shared.updateRecent(id: listData[indexPath.row].id)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DetailVC") as! DetailVC
        //let vc = DetailVC()
        vc.wordText = listData[indexPath.row].word
        vc.meanText = listData[indexPath.row].mean
        vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        self.navigationController?.pushViewController(vc, animated: true)
        //self.present(vc, animated: true, completion: nil)
        }
        
    }
