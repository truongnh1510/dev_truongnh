//
//  Define.swift
//  dev_nhtruong
//
//  Created by Huu Truong Nguyen on 2/7/22.
//

import Foundation
import UIKit

var readSpeed = UserDefaults.standard.value(forKey: "SPEED") as? Double
var letterSize = UserDefaults.standard.value(forKey: "SIZE") as? Double
let isIPad = UIDevice.current.userInterfaceIdiom == .pad
let appid = ""
